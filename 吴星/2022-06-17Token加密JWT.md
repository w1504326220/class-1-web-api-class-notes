## 下载包
dotnet add .\Soft1ApiDemo.Infrastructure\ package Microsoft.IdentityModel.Tokens

dotnet add .\Soft1ApiDEMO.Api\ package Microsoft.AspNetCore.Authentication.JwtBearer -v 3.1   

 //生成token
            var token = new JwtSecurityTokenHandler().WriteToken(securityToken);

## 路径是否加/users
当[Route("/token")]中有/不需要加/users
GET {{url}}/token HTTP/1.1
当[Route("token")]中没有/需要加/users
GET {{url}}/users/token HTTP/1.1